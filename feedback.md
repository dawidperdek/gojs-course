# Needs action

## Ogólne
- [ ] na pewno trzeba dodać te linki między lekcjami
- [ ] na pewno rozwiązać wszystkie todosy w readme i wszystkich lekcjach
- [ ] warto byłoby zmienić formy wszystkich zdań z "I" na "we", np. (lekcja 0) "However if there's something that you don't recognize, I advice..." na "... we advice..." (chociaż jak patrzę dalej to próbowaliście się tego trzymać, czyli po prostu wypadałoby zrobić jakieś review czysto językowe i wyłapać takie fragmenty, ewentualnie jakieś babole językowe, brakujące `the` i `a` itd.)

### Lekcja 0
- [ ] warto dodać mini sekcje o tym jak wypchnąć sobie projekt do publicznego repo we własnej przestrzeni na naszym bitbucket, żeby ktoś pilotujący wdrożenie mógł to łatwo i sprawnie sprawdzać i zachęcić do robienia wszystkiego przez pull requesty a nie na pałę do mastera
- [ ] można też wspomnieć o dodawaniu skryptu do konfiguracji w webstormie (w sensie, żeby przycisk play w górnym pasku odpalał od razu skrypt start, ale jeśli chcecie być IDE-agnostic to można olać ten punkt)

### Lekcja 1
- [ ] kodzik z dodawaniem layoutu w diagram.ts nie używa tego skrótu z `$ = go.graphobject.make`
- [ ] po dodaniu pliku `registerTemplates.ts` brakuje kroku, żeby dokleić wywołanie metody do `diagram.ts`
- [ ] notka o `Map` powinna być nad skrinem apki, a nie pod
- [ ] "It's just sugar" - tak powiedziane to chyba nie ma sensu, nie jestem pewien, ale chyba musi być "... syntactic sugar..." czy tam "syntax sugar"

### Lekcja 2
- [ ] brakuje mi tutaj tych komentarzy z nazwą pliku przy kodzikach
- [ ] `these lines` zamiast `This lines`
- [ ] trzeba dostarczyć nowe `mockData.ts`, bo w tym co wklejaliśmy w pierwszej lekcji nie ma pola `photo` w obiektach
- [ ] brakuje też pliku `src/gojs/consts/avatars.ts`
- [ ] te przyciski do dodawania dzieci to dodać do headera?
- [ ] w sumie to wszystkie buttony z tej i poprzedniej lekcji powinny mieć raczej typ `HTMLButtonElement` zamiast `HTMLInputElement`
- [ ] `In our family tree, we need to types of ports` - powinno być `two types`
- [ ] kodzik pod koniec, z dodaniem nazw portów do modelu, powinien mieć `;` a nie `,` na końcu linii
- [ ] brakuje też ikonki korony z pliku `src/gojs/consts/icons.ts`

### Lekcja 3
- [ ] po dodaniu roku urodzenia do linka obrazek pokazuje go w nawiasie, a dostarczony kodzik robi bez nawiasu

### Lekcja 4
- [ ] brakuje importu `gojs` w kodzie `groupTemplate.ts`
- [ ] od razu pod tym kodem powinno być "what we're dealing with" (o jedno "with" za dużo tam jest na początku)
- [ ] czy będzie gdzieś w dalszej części zwijanie/rozwijanie grup?

# Luźne uwagi i opinie
- od początku flow wydaje się być sensowny, jest prowadzone za rączkę elegancko
- dobrze, że przy każdym kodzie jest komentarz z nazwą pliku  (okazuje się, że nie przy każdym - trzeba o to zadbać)
- super, że do kwestii które są stopniowo wprowadzane są od razu linki do dokumentacji (np. panele, routing alhorithms, itd.)
- czysto technicznie: w takich sprawach
```typescript
const diagramDiv = document.getElementById('diagram');
const diagram = createDiagram(diagramDiv as HTMLDivElement);
```
warto zrobić
```typescript
const diagramDiv: HTMLDivElement = document.getElementById('diagram') as HTMLDivElement;
const diagram = createDiagram(diagramDiv);
```
bo przecież ten `diagramDiv` zawsze będzie `HTMLDivElement`, a skoro to wiemy, to zróbmy rzutowanie od razu wyciągając go, a nie przy przekazywaniu go gdzieś dalej
