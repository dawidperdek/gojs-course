import './styles.css';
import * as go from 'gojs';
import { createDiagram, loadDiagram, saveDiagram, setDiagramModel } from './gojs/diagram/diagram';

window.addEventListener('load', () => {
    const diagramDiv: HTMLDivElement = document.getElementById('diagram') as HTMLDivElement;
    const diagram: go.Diagram = createDiagram(diagramDiv);

    const saveButton: HTMLButtonElement = document.getElementById('saveButton') as HTMLButtonElement;
    saveButton.onclick = () => saveDiagram(diagram);

    const loadButton: HTMLButtonElement = document.getElementById('loadButton') as HTMLButtonElement;
    loadButton.onclick = () => setDiagramModel(diagram, loadDiagram());

    const initialDiagramModel = loadDiagram();
    if (initialDiagramModel) {
        setDiagramModel(diagram, initialDiagramModel);
    }

    const addGirlButton: HTMLButtonElement = document.getElementById('addGirlButton') as HTMLButtonElement;
    addGirlButton.onclick = () => diagram.model.addNodeData({ name: 'new girl', gender: 'F' });

    const addBoyButton: HTMLButtonElement = document.getElementById('addBoyButton') as HTMLButtonElement;
    addBoyButton.onclick = () => diagram.model.addNodeData({ name: 'new boy', gender: 'M' });
});
