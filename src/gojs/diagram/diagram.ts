import * as go from 'gojs';
import { addLayout } from './addLayout';
import { addModel } from './addModel';
import { registerCustomShapes, registerTemplates } from './registerTemplates';

const $ = go.GraphObject.make;

export const createDiagram = (diagramDiv: HTMLDivElement): go.Diagram => {
    const diagram = $(go.Diagram, diagramDiv);
    addModel(diagram);
    addLayout(diagram);
    registerCustomShapes(diagram);
    registerTemplates(diagram);
    return diagram;
};

export const saveDiagram = (diagram: go.Diagram): void => localStorage.setItem('diagram', diagram.model.toJson());

export const loadDiagram = (): go.Model => {
    const storedData = localStorage.getItem('diagram');
    return storedData ? go.Model.fromJson(storedData) : null;
};

export const setDiagramModel = (diagram: go.Diagram, model: go.Model): void => {
    diagram.model = model;
};
