import * as go from 'gojs';

import { createGroupTemplate } from '../templates/groupTemplate';
import { createLinkTemplate } from '../templates/linkTemplate';
import { createNodeTemplate } from '../templates/nodeTemplate';
import { registerMoreRoundedRectangle } from '../templates/customShapes';

export const registerCustomShapes = (diagram: go.Diagram): void => {
    registerMoreRoundedRectangle();
};

export const registerTemplates = (diagram: go.Diagram): void => {
    diagram.groupTemplateMap = new go.Map([
        { key: '', value: createGroupTemplate() }
    ]);
    diagram.nodeTemplateMap = new go.Map([
        { key: '', value: createNodeTemplate() }
    ]);
    diagram.linkTemplateMap = new go.Map([
        { key: '', value: createLinkTemplate() }
    ]);
};
