import * as go from 'gojs';
import { linkDataArray, nodeDataArrayWithGroups } from '../../mockData';

const $ = go.GraphObject.make;

export const addModel = (diagram: go.Diagram): void => {
    const model = new go.GraphLinksModel();
    model.nodeDataArray = nodeDataArrayWithGroups;
    model.linkDataArray = linkDataArray;
    model.linkFromPortIdProperty = 'parent';
    model.linkToPortIdProperty = 'child';
    diagram.model = model;
};
